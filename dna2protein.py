from Bio.Seq import Seq
import random

class SequenceFactory:
    @staticmethod
    def create_sequence(length, sequence_type='DNA'):
        if sequence_type == 'DNA':
            alphabet = ['A', 'C', 'G', 'T']
            sequence = ''.join(random.choice(alphabet) for _ in range(length))
            return Seq(sequence)
        elif sequence_type == 'protein':
            alphabet = "ACDEFGHIKLMNPQRSTVWY"
            sequence = ''.join(random.choice(alphabet) for _ in range(length))
            return Seq(sequence)
        else:
            raise ValueError('Invalid sequence type')

class transcription_and_translation:

    @staticmethod
    def transcribe_dna_to_rna(dna):
        result = dna.transcribe()
        return result

    @staticmethod
    def translate_rna_to_protein(rna):
        result = rna.translate()
        return result

class SequenceStorage:
    #Singleton design pattern
    _instance = None

    def __new__(cls,*args, **kwargs):
        if not cls._instance:
            cls._instance = super(SequenceStorage, cls).__new__(cls, *args, *kwargs)
            cls._instance.data = {}
        return cls._instance
    
    #def __init__(self):
    #    self.data = {}

    def save(self, name, seq):
        self.data[name] = seq

    def read(self, name):
        return self.data[name]
    
class DNASequenceGenerator:
    alphabet = ['A','C','G','T']
    def create_sequence(self, n):
        result = ''
        for i in range(n):
            idx = random.randint(0,3)
            result = result + DNASequenceGenerator.alphabet[idx]
        return result

if __name__ == '__main__':

    sequence = 'GTGGCCATTGTAATGGGCCGCTGAAAGGGTGCCCGATAG'

    dna = Seq(sequence)
      
    rna = transcription_and_translation.transcribe_dna_to_rna(dna)
    print('RNA sequence: ', rna)

    protein = transcription_and_translation.translate_rna_to_protein(rna)
    print('Protein sequence: ', protein)

    #Store generated sequences
    storage = SequenceStorage()
    storage.save('rna', rna)
    storage.save('protein', protein)

    #Generate a random sequence
    random_sequence_generator = DNASequenceGenerator()
    random_sequence = random_sequence_generator.create_sequence(50)